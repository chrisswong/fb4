//
//  FBFriendTableViewCell.h
//  fb4
//
//  Created by Chris on 30/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Facebook-iOS-SDK/FBSDKCoreKit/FBSDKProfilePictureView.h>

@interface FBFriendTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet FBSDKProfilePictureView *profileView;
@property (weak, nonatomic) IBOutlet UILabel *friendNameLabel;

- (void) configuareCellWithDictionary:(NSDictionary *) dict;

@end
