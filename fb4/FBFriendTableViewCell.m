//
//  FBFriendTableViewCell.m
//  fb4
//
//  Created by Chris on 30/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "FBFriendTableViewCell.h"

static NSString *FBIdKey = @"id";
static NSString *FBNameKey = @"name";

@implementation FBFriendTableViewCell

- (void) awakeFromNib {
    [super awakeFromNib];
}

- (void) configuareCellWithDictionary:(NSDictionary *) dict {
    self.profileView.profileID = dict[FBIdKey];
    self.friendNameLabel.text = dict[FBNameKey];
}

@end
