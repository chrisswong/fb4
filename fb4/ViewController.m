//
//  ViewController.m
//  fb4
//
//  Created by Chris on 27/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "ViewController.h"
#import <Facebook-iOS-SDK/FBSDKCoreKit/FBSDKCoreKit.h>
#import <Facebook-iOS-SDK/FBSDKLoginKit/FBSDKLoginKit.h>

#import <Facebook-iOS-SDK/FBSDKShareKit/FBSDKShareKit.h>

#import <MobileCoreServices/MobileCoreServices.h>

@interface ViewController () <UIActionSheetDelegate,FBSDKSharingDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *currentStatusLabel;
@property (weak, nonatomic) IBOutlet UIView *likeButtonContainer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fbTokenDidChange:) name:FBSDKAccessTokenDidChangeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileDidChange:) name:FBSDKProfileDidChangeNotification object:nil];
    
//    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//    content.contentURL = [NSURL URLWithString:@"https://developers.facebook.com"];
//    FBSDKSendButton *button = [[FBSDKSendButton alloc] init];
//    button.shareContent = content;
//    [self.likeButtonContainer addSubview:button];
//    
//    FBSDKLikeControl
    
    FBSDKLikeControl *button = [[FBSDKLikeControl alloc] init];
    button.objectID = @"https://www.facebook.com/FacebookDevelopers";
    [self.likeButtonContainer addSubview:button];
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) profileDidChange:(NSNotification *) notification {
    FBSDKProfile *currentProfile = [FBSDKProfile currentProfile];
    NSString *message = [NSString stringWithFormat:@"user id : %@\nUser name : %@", currentProfile.userID, currentProfile.name];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

- (void) fbTokenDidChange:(NSNotification *) notification {
    if ([FBSDKAccessToken currentAccessToken]) {
        //logged in

    } else {
        //logged out
//        NSString *message = [NSString stringWithFormat:@"user id : %@\nUser name : %@", currentProfile.userID, currentProfile.name];
        NSString *message = @"You have logged out";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
}

#pragma mark - Delegate

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            
        {
            FBSDKShareLinkContent *a = [[FBSDKShareLinkContent alloc] init];
            a.contentURL = [NSURL URLWithString:@"https://developers.facebook.com"];
            
            [FBSDKShareDialog showFromViewController:self withContent:a delegate:self];
        }
            break;
        case 1:
        {
            FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
            photo.image = [UIImage imageNamed:@"watch.png"];
            photo.userGenerated = NO;

            FBSDKSharePhotoContent *photoContent = [FBSDKSharePhotoContent new];
            photoContent.photos = @[photo];
            
            [FBSDKShareDialog showFromViewController:self
                                         withContent:photoContent
                                            delegate:self];
        }
            
            break;
        case 2:
        {
            
            if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]) {
                
                UIImagePickerController *imagePickerViewController = [[UIImagePickerController alloc] init];
                imagePickerViewController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                imagePickerViewController.delegate = self;
                imagePickerViewController.mediaTypes = @[(id) kUTTypeMovie];
                
                [self presentViewController:imagePickerViewController animated:YES completion:NULL];
            }
            

        }
            break;
        case 3:
            break;
    }
}

#pragma mark - Actions

- (IBAction)facebookAppSharing {
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Choose one to share via FB app" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Link",@"Photo",@"Video", nil];
    [sheet showInView:self.view];
}

- (IBAction)logout {
    FBSDKLoginManager *logOut = [[FBSDKLoginManager alloc] init];
    [logOut logOut];
}

- (IBAction)login {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"%@" , error.localizedDescription);
        } else {
            
            if (result.isCancelled) {
                NSLog(@"user cancelled login!");
            } else {
//                NSLog(@"%@", result.grantedPermissions);
//                NSLog(@"token = %@", result.token);
            }

        }
    }];
}
- (IBAction)loginWithPublishModel {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithPublishPermissions:@[@"publish_actions"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"%@" , error.localizedDescription);
        } else {
            
            if (result.isCancelled) {
                NSLog(@"user cancelled login!");
            } else {
                //                NSLog(@"%@", result.grantedPermissions);
                //                NSLog(@"token = %@", result.token);
            }
            
        }
    }];
    
}

#pragma mark - UIImagePickerViewController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if (info[UIImagePickerControllerMediaURL]) {
        FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
        video.videoURL = info[UIImagePickerControllerReferenceURL];
        FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
        content.video = video;

        [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
    }
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Share Dialog delegate

- (void) sharerDidCancel:(id<FBSDKSharing>)sharer {
    
}

- (void) sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"%@" , error.localizedDescription);
}


- (void) sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"%@" , results);
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:NULL];
}
@end
