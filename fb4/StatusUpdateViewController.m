//
//  StatusUpdateViewController.m
//  fb4
//
//  Created by Chris on 27/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "StatusUpdateViewController.h"
#import <FBSDKCoreKit/FBSDKGraphRequest.h>

@interface StatusUpdateViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation StatusUpdateViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    [tapGestureRecognizer addTarget:self action:@selector(viewDidTap)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void) viewDidTap {
    [self.view endEditing:YES];
}

- (IBAction)publish {
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/feed"
                                                                   parameters:@{@"message": self.textView.text}
                                                                   HTTPMethod:@"POST"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        
        if (!error) {
            NSLog(@"%@", error.localizedDescription);
        } else {
            NSLog(@"%@" , result);
        }
    }];
}

@end
