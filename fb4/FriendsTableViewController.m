//
//  FriendsTableViewController.m
//  fb4
//
//  Created by Chris on 30/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "FriendsTableViewController.h"

#import "FBFriendTableViewCell.h"
#import <FBSDKCoreKit/FBSDKGraphRequest.h>

static NSString *FBFriendTableViewCellIdentifier = @"FBFriendTableViewCellIdentifier";



@interface FriendsTableViewController()

@property (nonatomic, strong) NSArray *friends;

@end

@implementation FriendsTableViewController

#pragma mark - LifeCycle

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/friends/" parameters:nil HTTPMethod:@"GET"];
    
    __weak typeof(self)weakSelf = self;
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (!error) {
            
            NSDictionary *results = (NSDictionary *) result;
            strongSelf.friends = results[@"data"];
            [strongSelf.tableView reloadData];
            
        } else {
            
        }
    }];
    
    
}

#pragma mark - UITableViewDataSource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.friends count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FBFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FBFriendTableViewCellIdentifier];
    [cell configuareCellWithDictionary:self.friends[indexPath.row]];
    return cell;
}

@end
